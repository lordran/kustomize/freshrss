#!/bin/sh

work_dir=$(dirname $0)

SALT=$(pass perso/Servers/FreshRSS/salt) \
    POSTGRES_PASSWORD=$(pass perso/Servers/FreshRSS/pgsql) \
    envsubst > $work_dir/config.php < $work_dir/config.php.tmpl
